const io = require('socket.io-client');
const VERSION_COMPATIBILITY = "0.9.15";
/*const {findLocalTriggerboxes} = require("./standaloneTriggerbox/isolatedTB")*/

const { fork } = require('child_process');


module.exports = class {


    constructor(verbose = true, endpoint = 'http://localhost', port = 50005){
        this.endpoint = endpoint+':'+port;
        this.port = port;
        this.server='';
        this.availableLights = [];
        this.triggerboxes = [];
        this.connected = false;
        this.verbose = verbose;
        this.socket = null;
    };

    TriggerBoxEnums={
        outputMode:{
            cc:0,
            ttl3v3:1,
            ttl5v0:2
        }
    }

    /*
    *   Bootstraps the Esper API wrapper
    * */
    connect(){
        return new Promise((resolve,reject)=>{
            this.socket = io(this.endpoint);
            if(this.verbose){console.log("Connecting to Esper API at "+this.endpoint);}
            this.socket.on('connect',()=>{
                if(this.verbose){console.log("Connected to Esper API");}
                if(this.verbose){console.log("API wrapper version: " + VERSION_COMPATIBILITY);}
                this.connected = true;
                this.getAvailableLights()
                // .then((theLights)=>{ // TJG Commented out as available lights already set in above function
                //     this.availableLights = theLights;
                // })
                    .then(()=>{
                        return this.getControlSuiteVersion();
                    }).then((versionString)=>{
                    versionString = versionString.replace("v", "");
                    if (versionString !== VERSION_COMPATIBILITY){
                        // console.warn("WARN: api version and ControlSuite version mismatch");
                        // console.warn("Please update api/control suites to equal versions");
                    }
                }).then(()=>{
                    resolve();
                })
                    .catch((err)=>{
                        console.log(err);
                        console.err("Connection error");
                        reject(['failed to start up correctly - could not detect connected lights'])
                    });

            });

            this.socket.on('disconnect',()=>{
                if(this.verbose){console.log("Disconnected from Esper API");}
                this.connected = false;
            });

            this.socket.on('userMessage',(payload,callback)=>{
                if(this.verbose){
                    console.log(payload.message);
                }
                callback(payload.id);
            })
        });
    }


    detectLocalTriggerboxes(){
        return new Promise((resolve,reject)=>{
            this.socket = {
                emit:(toEmit,payload)=>{
                    let toSend = {
                        handle:toEmit,
                        payload:{}
                    };
                    if(typeof payload === "function"){
                        toSend.payload = JSON.stringify(payload);
                    }
                    console.log(`emitting ${toEmit}`);
                    this.socket.thread.send(JSON.stringify(toSend));
                },
                thread:fork('./standaloneTriggerbox/isolatedTB.js')
            }
            this.socket.thread.on("message",(msg)=>{
                console.log("got message " + msg)
                if(msg === "found-triggerbox"){
                    resolve();
                }
            })
            setTimeout(reject,10000);
        })

        // return new Promise((resolve,reject)=>{
        //     //const triggerboxController = fork('./api-core/standaloneTriggerbox/isolatedTB.js');
        //     //this.socket = io(this.endpoint);
        //     const socket = io("http://localhost:40004");
        //     console.log("Connecting to Esper API at http://localhost:40004");
        //     socket.on('connect',()=>{
        //         console.log("Connected to Esper");
        //         this.connected = true;
        //         resolve();
        //     });
        //
        //     socket.on('disconnect',()=>{
        //         if(this.verbose){console.log("Disconnected from Esper");}
        //         this.connected = false;
        //     });
        //
        //
        //
        //
        //
        // });
    }



    /*
    *   Polls the API for the lights that the controller box can communicate with
    * */
    getAvailableLights(){
        return new Promise((resolve,reject)=>{
            if(this.connected){
                this.socket.emit('api-get-available-lights',(payload)=>{
                    if(payload.status){
                        if (payload.lights){ // not preset if starting api/controlsuite without using config.
                            if(this.verbose){console.log(`Detected available lights - there are ${payload.lights.length} lights`);}
                            this.availableLights = payload.lights;
                            resolve(payload.lights);
                        }
                    }else{
                        reject(payload.errors);
                    }
                });
            }else{
                reject(['Could not get available lights - Not connected to Esper API']);
            }
        });
    }

    nodeExists(id){
        let connectedNodes= [];
        for(let i=0;i<this.availableLights.length;i++){
            connectedNodes.push(this.availableLights[i].id);
        }
        return connectedNodes.includes(id);
    }


    /*
    *   Turns on the modelling light - steady state lighting for each light node
    * */
    globalModellingLight(payload){
        return new Promise((resolve,reject)=> {
            if (this.connected) {
                //console.log("Setting global modelling light...");
                let allOk = true;
                let desiredGlobalModellingLight = [];
                let errorMessages = [];
                if (payload.length !== 3) {
                    allOk = false;
                    errorMessages.push('Expected an intensity array of length 3');
                }
                if (typeof payload[0] !== 'number' || typeof payload[1] !== 'number' || typeof payload[2] !== 'number') {
                    allOk = false;
                    errorMessages.push('Light intensities should be a number');
                } else {
                    desiredGlobalModellingLight = [
                        (Math.round(parseFloat(payload[0]) * 10000) / 10000),
                        (Math.round(parseFloat(payload[1]) * 10000) / 10000),
                        (Math.round(parseFloat(payload[2]) * 10000) / 10000),
                    ];
                    if (desiredGlobalModellingLight[0] < 0 || desiredGlobalModellingLight[0] > 3) {
                        allOk = false;
                        errorMessages.push('Intensity should be between 0 and 3%');
                    }
                    if (desiredGlobalModellingLight[1] < 0 || desiredGlobalModellingLight[1] > 3) {
                        allOk = false;
                        errorMessages.push('Intensity should be between 0 and 3%');
                    }
                    if (desiredGlobalModellingLight[2] < 0 || desiredGlobalModellingLight[2] > 3) {
                        allOk = false;
                        errorMessages.push('Intensity should be between 0 and 3%');
                    }
                }
                if (allOk) {
                    this.socket.emit('api-set-global-modelling-light', desiredGlobalModellingLight, (response) => {
                        if (response.status) {
                            resolve(desiredGlobalModellingLight);
                        } else {
                            reject(response.errors);
                        }
                    });
                } else {
                    reject(errorMessages)
                }
            }
        });
    }

    setLoopAtStage(stage){
        if(Number.isInteger(stage)){
            if(stage > 1){
                this.useBackendTerminalCommand(`slas ${stage}`);      //set loop at stage
            }else{
                console.log("must set loop at to a number greater than 1")
            }
        }
    }

    setLoopToStage(stage){
        this.useBackendTerminalCommand(`slts ${stage}`);      //set loop at stage
    }

    driveMVForNFrames(count,frequency,flashOffset){
        if(
            Number.isInteger(count)  &&
            Number.isInteger(frequency)  &&
            Number.isInteger(flashOffset)
        ){
            this.useBackendTerminalCommand(`mvg ${count} ${frequency} ${flashOffset}`);
        }else{
            console.log("driveMVForNFrames requires interger values");
        }
    }

    startMvDrivenCapture(cc){
        if(cc){
            this.useBackendTerminalCommand("PT 1");
        }else{
            this.useBackendTerminalCommand("tsen");
            this.useBackendTerminalCommand("tsog");
        }
    }

    stopMVDrivenCapture(cc){
        if(cc){
            this.useBackendTerminalCommand("PT 0");
        }else{
            this.useBackendTerminalCommand("tscg");
            this.useBackendTerminalCommand("tsdis");
        }

    }

    individualModellingLight(payload) {
        return new Promise((resolve,reject)=>{

            let allOk = true;
            let errorMessages = [];

            if (payload.intensities.length !== 3) {
                allOk = false;
                errorMessages.push(`Expecting intensities to be an array of length 3`)
            }

            if (typeof (payload.intensities[0]) !== 'number') {
                allOk = false;
                errorMessages.push(`Light intensities should be a number`);
            }
            if (typeof (payload.intensities[1]) !== 'number') {
                allOk = false;
                errorMessages.push(`Light intensities should be a number`);
            }
            if (typeof (payload.intensities[2]) !== 'number') {
                allOk = false;
                errorMessages.push(`Light intensities should be a number`);
            }

            payload.intensities[0] = (Math.round(parseFloat(payload.intensities[0]) * 10000) / 10000);
            payload.intensities[1] = (Math.round(parseFloat(payload.intensities[1]) * 10000) / 10000);
            payload.intensities[2] = (Math.round(parseFloat(payload.intensities[2]) * 10000) / 10000);
            if (payload.intensities[0] < 0 || payload.intensities[0] > 3) {
                allOk = false;
                errorMessages.push(`Could not set individual modelling light -  intensity should be between 0 and 3%`);
            }
            if (payload.intensities[1] < 0 || payload.intensities[1] > 3) {
                allOk = false;
                errorMessages.push(`Could not set individual modelling light -  intensity should be between 0 and 3%`);
            }
            if (payload.intensities[2] < 0 || payload.intensities[2] > 3) {
                allOk = false;
                errorMessages.push(`Could not set individual modelling light -  intensity should be between 0 and 3%`);
            }

            if (!this.nodeExists(payload.id)) {
                allOk = false;
                errorMessages.push(`ID passed does not exist - id ${payload.id}`)
            }

            if (allOk) {
                this.socket.emit('api-set-individual-modelling-light', payload, (response) => {
                    if (response.status) {
                        resolve();
                    } else {
                        reject(response.errors);
                    }
                });
            } else {
                reject(errorMessages);
            }
        });
    }

    /*
    *   Turns the modelling light off - sets a zero brightness for each LED globally
    * */
    modellingLightOff(){
        return new Promise((resolve,reject)=>{
            this.globalModellingLight([0,0,0])
                .then(resolve)
                .catch((err)=>{reject(err)});
        });
    }

    triSyncEnableModule(){
        return new Promise((resolve, reject)=>{
            this.socket.emit("tri-sync-enable-module", (response)=>{
                if (response === true){
                    resolve();
                }
            });
        })
    }

    triSyncDisableModule(){
        return new Promise((resolve, reject)=>{
            this.socket.emit("tri-sync-disable-module", (response)=>{
                if (response === true){
                    resolve();
                }
            });
        })
    }

    triSyncOpenGate(){
        return new Promise((resolve, reject)=>{
            this.socket.emit("tri-sync-open-gate", (response)=>{
                if (response === true){
                    resolve();
                }
            });
        })
    }

    triSyncCloseGate(){
        return new Promise((resolve, reject)=>{
            this.socket.emit("tri-sync-close-gate", (response)=>{
                if (response === true){
                    resolve();
                }
            });
        })
    }

    triSyncSetFlashLag(newFlashLag_250dms){
        return new Promise((resolve, reject)=>{
            this.socket.emit("tri-sync-set-flash-lag", (response)=>{
                if (response === true){
                    resolve();
                }
            });
        })
    }

    bullseyeOn(){
        return new Promise((resolve,reject)=>{
            this.socket.emit('assertBullseye',true,(response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.errors);
                }
            });
        });
    }
    bullseyeOff(){
        return new Promise((resolve,reject)=>{
            this.socket.emit('assertBullseye',false,(response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.errors);
                }
            });
        });
    }

    /**
     *
     * @returns {Promise<string>}
     */
    getControlSuiteVersion(){
        return new Promise((resolve, reject)=>{
            this.socket.emit("api-get-version-number", (response)=>{
                if (typeof response === "string"){
                    console.log("Control Suite Version: " + response);
                    resolve(response);
                }
                else {
                    reject("no version specified...");
                }
            })
        });
    }

    setLightingMode(mode){
        return new Promise((resolve, reject)=>{
            console.log("Setting lighting mode to: " + mode);
            if (mode === "table" || mode === "chain"){
                this.socket.emit("api-set-lighting-mode", mode, (response)=>{
                    if (response.status === true){
                        console.log("New lighting mode set: " + mode);
                        resolve();
                    }
                    else if (response.progress !== undefined){
                        console.log("progress: " + response.progress + "%");
                    }
                    else if (response.status === false){
                        reject("ControlSuite did not confirm mode change");
                    }
                });
            }
            else{
                reject("Lighting mode arg must be one of \"table\" or \"chain\"");
            }
        });
    }


    /**
     * Chain mode is for being able to take many photos - each light can only flash once
     * @param {ChainModeDataInstance[]} chainData
     * @returns {Promise<void>}
     */
    configureChainMode(chainData){
        return new Promise((resolve, reject) => {
            console.log("Configuring chain mode");
            let allOk = true;
            let idsUsed = [];
            let errorMessages = [];

            if (typeof chainData == "undefined"){
                allOk = false;
                errorMessages.push("chainData not defined");
                reject(errorMessages);
            }

            for (let i = 0; i < chainData.length; i++){

                //check there aren't duplicate ids
                if (idsUsed.includes(chainData[i].id)){
                    allOk = false;
                    errorMessages.push(`Duplicate Light IDs were passed - #${chainData[i].id} was passed multiple times`);
                }
                idsUsed.push(chainData[i].id);


                //check the id is associated with a node we have detected
                if (!this.nodeExists(chainData[i].id)){
                    allOk = false;
                    errorMessages.push(`Invalid Light ID was passed - ID ${chainData[i].id} does not exist`);
                }

                // //check we have the correct number of intensities passed
                // if(chainData[i].intensities.length !== 3){
                //     allOk = false;
                //     errorMessages.push(`Incorrect number of intensities passed - each stage should have 3 intensities - stage ${i+1} has ${chainData[i].intensities.length} set`);
                // }


                // //check the intensities are in range (0-100%)
                // // noinspection JSCheckFunctionSignatures
                // chainData[i].intensities[0] = (Math.round(parseFloat(chainData[i].intensities[0])*10000)/10000);
                // // noinspection JSCheckFunctionSignatures
                // chainData[i].intensities[1] = (Math.round(parseFloat(chainData[i].intensities[1])*10000)/10000);
                // // noinspection JSCheckFunctionSignatures
                // chainData[i].intensities[2] = (Math.round(parseFloat(chainData[i].intensities[2])*10000)/10000);
                //
                // if(chainData[i].intensities[0] < 0 || chainData[i].intensities[0] > 100 ){
                //     allOk = false;
                //     errorMessages.push(`Flash intensities must between 0 and 100 - occurred on stage ${i+1} light node ID ${chainData[i].id} - ${chainData[i].intensities[0]} passed`);
                // }
                // if(chainData[i].intensities[1] < 0 || chainData[i].intensities[1] > 100 ){
                //     allOk = false;
                //     errorMessages.push(`Flash intensities must between 0 and 100 - occurred on stage ${i+1} light node ID ${chainData[i].id} - ${chainData[i].intensities[1]} passed`);
                // }
                // if(chainData[i].intensities[2] < 0 || chainData[i].intensities[2] > 100 ){
                //     allOk = false;
                //     errorMessages.push(`Flash intensities must between 0 and 100 - occurred on stage ${i+1} light node ID ${chainData[i].id} - ${chainData[i].intensities[2]} passed`);
                // }
                // //check the flash positions are within range
                // if(chainData[i].flashPosition > 255 || chainData[i].flashPosition < 0 ){
                //     allOk = false;
                //     errorMessages.push(`Flash position must be between 0 and  255 - occurred on stage ${i+1} light node ID ${chainData[i].id} - ${chainData[i].flashPosition} passed`);
                // }

                // check flash duration is a number
                if (typeof chainData[i].duration !== 'number'){
                    allOk = false;
                    errorMessages.push(`Flash duration must be a number - occurred on stage ${i + 1} light node ID # ${chainData[i].id}`);

                }
                //check the flash durations are acceptable
                if (chainData[i].duration < 0 || chainData[i].duration > 30){
                    allOk = false;
                    errorMessages.push(`Duration error - flash duration must be between 0 and 30 - occurred on stage ${i + 1} light node ID ${chainData[i].id} - ${chainData[i].duration} passed`);
                }
            }
            if (allOk){
                this.socket.emit('api-configure-chain-mode-sequence', chainData, (response) => {
                    if (response.status){
                        console.log("ControlSuite configured chain mode....");
                        resolve();
                    }
                    else{
                        console.log("ControlSuite responded with errors:");
                        reject(response.errors);
                    }
                });
            }
            else{
                reject(errorMessages);
            }
        });
    }

    /**
     *
     * @param {number[]} brightnesses
     * @returns {Promise<void>}
     */
    armChainMode(brightnesses){
        return new Promise(((resolve, reject) => {
                if (Array.isArray(brightnesses)){
                    /** @type {boolean} */ let allNums = true;
                    for (let bval of brightnesses){
                        if (typeof bval !== "number"){
                            allNums = false;
                        }
                    }
                    if (allNums){
                        this.socket.emit("api-arm-chain-mode-sequence", brightnesses, (response) => {
                            if (response.status){
                                resolve();
                            }
                            else{
                                // noinspection JSUnresolvedVariable
                                reject(response.errors);
                            }
                        });
                    }
                }
            }
        ));
    }

    setLoopToStage(loopTo) {
        return new Promise((resolve, reject) => {
            console.log("Setting loopTo stage: " + loopTo);
            if (typeof loopTo === "number") {
                this.socket.emit("api-set-loop-to-stage", loopTo, (response) =>{
                    if (response.status) {
                        if (response.status === true) {
                            console.log("LoopTo stage set...");
                            this.socket.emit("api-set-current-sequence-position", loopTo, (response) => {
                                    if (response.status === true) {
                                        resolve();
                                    }
                                }
                            );
                        }
                    } else {
                        reject("No response object");
                    }
                });
            } else {
                reject(["payload.loopToStage must be an integer in the range 0-31"]);
            }
        });
    }

    setLoopAtStage(loopAt) {
        return new Promise((resolve, reject) => {
            console.log("Setting loopAt stage: " + loopAt);
            if (typeof loopAt === "number") {
                this.socket.emit("api-set-loop-at-stage", loopAt, (response)=>{
                    if (response.status){
                        if (response.status === true){
                            console.log("LoopAt stage set...");
                            resolve();
                        }
                    }
                    else{
                        reject("No response object");
                    }
                });
            }
            else {
                reject(["payload.loopAtStage must be an integer in the range 0-31"]);
            }
        });
    }

    /**
     *
     * @param sequenceData
     * @returns {Promise<void>}
     */
    sequence(sequenceData,compensateForFilterLoss){
        return new Promise((resolve,reject)=>{
            console.log("Uploading Sequence payload");
            let allOk = true;
            let errorMessages = [];
            // check number of stages
            if(sequenceData.length > 32){
                allOk = false;
                errorMessages.push('Too many stages - exceeded 32 light stages. - tried to set '+sequenceData.length+' stages');
            }

            if(!Array.isArray(sequenceData)){
                allOk = false;
                errorMessages.push('Sequence Data must be an array');
            }

            for(let i=0;i<sequenceData.length;i++){
                if(!Array.isArray(sequenceData[i])){
                    allOk = false;
                    errorMessages.push(`Each stage within the sequence data must be an array - stage ${i+1} was an ${typeof sequenceData[i]}`);
                }

                let idsUsed = [];
                for(let j=0;j<sequenceData[i].length;j++){
                    //check ids are unique
                    if(idsUsed.includes(sequenceData[i][j].id)){
                        allOk = false;
                        errorMessages.push(`Duplicate light ID in stage ${i+1} - id # ${sequenceData[i][j].id} duplicated`);
                    }
                    idsUsed.push(sequenceData[i][j].id);
                    // check id exists
                    if(!this.nodeExists(sequenceData[i][j].id)) {
                        allOk = false;
                        errorMessages.push(`Invalid Light ID was passed - ID ${sequenceData[i][j].id} does not exist`);
                    }


                    // check flash duration is a number
                    if(typeof sequenceData[i][j].duration !== 'number'){
                        allOk = false;
                        errorMessages.push(`Flash duration must be a number - occurred on stage ${i+1} light node ID # ${sequenceData[i][j].id}`);
                    }

                    // check duration is in range
                    if(sequenceData[i][j].duration <= 0 || sequenceData[i][j].duration > 30){
                        allOk=false;
                        errorMessages.push(`Duration error - 0 < flash duration <= 30 - occurred on stage ${i+1} light node ID ${sequenceData[i][j].id}`)
                    }

                    //check correct number of intensities
                    if(sequenceData[i][j].intensities.length !== 3){
                        allOk = false;
                        errorMessages.push(`Incorrect number of intensities sent - expected 3, received ${sequenceData[i][j].intensities.length} - occurred on stage ${i+1} light node ID ${sequenceData[i][j].id}`)
                    }

                    //check intensities are a number
                    if(typeof sequenceData[i][j].intensities[0] !== 'number'){
                        allOk = false;
                        errorMessages.push(`Flash intensities must be a number - occurred on stage ${i+1} light node ID ${sequenceData[i][j].id}`)
                    }
                    if(typeof sequenceData[i][j].intensities[1] !== 'number'){
                        allOk = false;
                        errorMessages.push(`Flash intensities must be a number - occurred on stage ${i+1} light node ID ${sequenceData[i][j].id}`)
                    }
                    if(typeof sequenceData[i][j].intensities[2] !== 'number'){
                        allOk = false;
                        errorMessages.push(`Flash intensities must be a number - occurred on stage ${i+1} light node ID ${sequenceData[i][j].id}`)
                    }

                    //check intensities are in range
                    sequenceData[i][j].intensities[0] = (Math.round(parseFloat(sequenceData[i][j].intensities[0])*100)/100);
                    sequenceData[i][j].intensities[1] = (Math.round(parseFloat(sequenceData[i][j].intensities[1])*100)/100);
                    sequenceData[i][j].intensities[2] = (Math.round(parseFloat(sequenceData[i][j].intensities[2])*100)/100);
                    if(sequenceData[i][j].intensities[0] < 0 || sequenceData[i][j].intensities[0] > 100){
                        allOk = false;
                        errorMessages.push(`Flash intensities must between 0 and 100 - sent ${sequenceData[i][j].intensities[0]} - occurred on stage ${i+1} light node ID ${sequenceData[i][j].id}`)
                    }
                    if(sequenceData[i][j].intensities[1] < 0 || sequenceData[i][j].intensities[1] > 100){
                        allOk = false;
                        errorMessages.push(`Flash intensities must between 0 and 100 - sent ${sequenceData[i][j].intensities[1]} - occurred on stage ${i+1} light node ID ${sequenceData[i][j].id}`)
                    }
                    if(sequenceData[i][j].intensities[2] < 0 || sequenceData[i][j].intensities[2] > 100){
                        allOk = false;
                        errorMessages.push(`Flash intensities must between 0 and 100 - sent ${sequenceData[i][j].intensities[2]} - occurred on stage ${i+1} light node ID ${sequenceData[i][j].id}`)
                    }

                }

            }

            if(allOk){
                this.socket.emit('api-upload-sequencer-mode-sequence',sequenceData,!!compensateForFilterLoss,(response) => {
                    if (response.status) {
                        resolve();
                    } else {
                        reject(response.errors);
                    }
                });
            }else{
                reject(errorMessages);
            }
        });

    }


    extendedSequence(sequenceData){
        return new Promise((resolve,reject)=>{
            console.log("Uploading extended Sequence payload");
            let allOk = true;
            let errorMessages = [];
            // check number of stages
            if(sequenceData.length > 160){
                allOk = false;
                errorMessages.push('Too many stages - exceeded 160 light stages. - tried to set '+sequenceData.length+' stages');
            }

            if(!Array.isArray(sequenceData)){
                allOk = false;
                errorMessages.push('Sequence Data must be an array');
            }

            for(let i=0;i<sequenceData.length;i++){
                if(!Array.isArray(sequenceData[i])){
                    allOk = false;
                    errorMessages.push(`Each stage within the sequence data must be an array - stage ${i+1} was an ${typeof sequenceData[i]}`);
                }

                let idsUsed = [];
                for(let j=0;j<sequenceData[i].length;j++){
                    //check ids are unique
                    if(idsUsed.includes(sequenceData[i][j].id)){
                        allOk = false;
                        errorMessages.push(`Duplicate light ID in stage ${i+1} - id # ${sequenceData[i][j].id} duplicated`);
                    }
                    idsUsed.push(sequenceData[i][j].id);
                    // check id exists
                    if(!this.nodeExists(sequenceData[i][j].id)) {
                        allOk = false;
                        errorMessages.push(`Invalid Light ID was passed - ID ${sequenceData[i][j].id} does not exist`);
                    }


                    // check flash duration is a number
                    if(typeof sequenceData[i][j].duration !== 'number'){
                        allOk = false;
                        errorMessages.push(`Flash duration must be a number - occurred on stage ${i+1} light node ID # ${sequenceData[i][j].id}`);
                    }

                    // check duration is in range
                    if(sequenceData[i][j].duration <= 0 || sequenceData[i][j].duration > 30){
                        allOk=false;
                        errorMessages.push(`Duration error - 0 < flash duration <= 30 - occurred on stage ${i+1} light node ID ${sequenceData[i][j].id}`)
                    }

                    //check correct number of intensities
                    if(sequenceData[i][j].intensities.length !== 3){
                        allOk = false;
                        errorMessages.push(`Incorrect number of intensities sent - expected 3, received ${sequenceData[i][j].intensities.length} - occurred on stage ${i+1} light node ID ${sequenceData[i][j].id}`)
                    }

                    //check intensities are a number
                    if(typeof sequenceData[i][j].intensities[0] !== 'number'){
                        allOk = false;
                        errorMessages.push(`Flash intensities must be a number - occurred on stage ${i+1} light node ID ${sequenceData[i][j].id}`)
                    }
                    if(typeof sequenceData[i][j].intensities[1] !== 'number'){
                        allOk = false;
                        errorMessages.push(`Flash intensities must be a number - occurred on stage ${i+1} light node ID ${sequenceData[i][j].id}`)
                    }
                    if(typeof sequenceData[i][j].intensities[2] !== 'number'){
                        allOk = false;
                        errorMessages.push(`Flash intensities must be a number - occurred on stage ${i+1} light node ID ${sequenceData[i][j].id}`)
                    }

                    //check intensities are in range
                    sequenceData[i][j].intensities[0] = (Math.round(parseFloat(sequenceData[i][j].intensities[0])*100)/100);
                    sequenceData[i][j].intensities[1] = (Math.round(parseFloat(sequenceData[i][j].intensities[1])*100)/100);
                    sequenceData[i][j].intensities[2] = (Math.round(parseFloat(sequenceData[i][j].intensities[2])*100)/100);
                    if(sequenceData[i][j].intensities[0] < 0 || sequenceData[i][j].intensities[0] > 100){
                        allOk = false;
                        errorMessages.push(`Flash intensities must between 0 and 100 - sent ${sequenceData[i][j].intensities[0]} - occurred on stage ${i+1} light node ID ${sequenceData[i][j].id}`)
                    }
                    if(sequenceData[i][j].intensities[1] < 0 || sequenceData[i][j].intensities[1] > 100){
                        allOk = false;
                        errorMessages.push(`Flash intensities must between 0 and 100 - sent ${sequenceData[i][j].intensities[1]} - occurred on stage ${i+1} light node ID ${sequenceData[i][j].id}`)
                    }
                    if(sequenceData[i][j].intensities[2] < 0 || sequenceData[i][j].intensities[2] > 100){
                        allOk = false;
                        errorMessages.push(`Flash intensities must between 0 and 100 - sent ${sequenceData[i][j].intensities[2]} - occurred on stage ${i+1} light node ID ${sequenceData[i][j].id}`)
                    }

                }

            }

            if(allOk){
                //endpoint is agnostic, it doesnt care 32 vs 160 so send it anyway
                this.socket.emit('api-upload-sequencer-mode-sequence',sequenceData,(response) => {
                    if (response.status) {
                        resolve();
                    } else {
                        reject(response.errors);
                    }
                });
            }else{
                reject(errorMessages);
            }
        });

    }
    /*
    *   The command to tell the rig to start the capture. Fine-grained control over number of stages to take
    * */
    trigger(args = {}){
        return new Promise((resolve,reject)=>{
            let errorMessages = [];
            let allOk = true;
            let triggerPayload = {};

            if(args.hasOwnProperty('stages')){
                if(typeof (args.stages) === 'number' && !(args.stages%1)){
                    triggerPayload.stages = args.stages;
                }else{
                    allOk = false;
                    errorMessages.push('stages must be an integer');
                }
            }else{
                triggerPayload.stages = 0;
            }

            if(args.hasOwnProperty('fps')){
                if(typeof (args.fps) === 'number' ){
                    if( args.fps >= 0){
                        triggerPayload.fps = args.fps;
                    }else{
                        allOk = false;
                        errorMessages.push('fps must be above 0');
                    }
                }else{
                    allOk = false;
                    errorMessages.push('fps must be a number');
                }
            }else{
                triggerPayload.fps = 0;
            }

            if(args.hasOwnProperty('start')){
                if(typeof (args.start) === 'number' && !(args.start%1)){
                    if( args.start >=0){
                        triggerPayload.start = args.start;
                    }else{
                        allOk = false;
                        errorMessages.push('start must be greater  >=0 - '+args.start+' given');
                    }
                }else{
                    allOk = false;
                    errorMessages.push('start must be an integer');
                }
            }else{
                triggerPayload.start = 0;
            }


            if(args.hasOwnProperty('acclimatizationDuration')){
                if(typeof (args.acclimatizationDuration) === 'number' && !(args.acclimatizationDuration%1)){
                    if( args.acclimatizationDuration <= 10000 && args.acclimatizationDuration >=0){
                        triggerPayload.acclimatizationDuration = args.acclimatizationDuration;
                    }else{
                        allOk = false;
                        errorMessages.push('Acclimatization duration must be between 0 and 10000 - '+args.acclimatizationDuration+' given');
                    }
                }else{
                    allOk = false;
                    errorMessages.push('Acclimatization duration must be an integer');
                }
            }else{
                triggerPayload.acclimatizationDuration = 0;
            }


            if(args.hasOwnProperty('acclimatizationIntensity')){
                if(typeof (args.acclimatizationIntensity) === 'number'){
                    if( args.acclimatizationIntensity <= 10 && args.acclimatizationIntensity >=0){
                        triggerPayload.acclimatizationIntensity = args.acclimatizationIntensity;
                    }else{
                        allOk = false;
                        errorMessages.push('Acclimatization intensity must be between 0 and 10 - '+args.acclimatizationIntensity+' given');
                    }
                }else{

                    allOk = false;
                    errorMessages.push('Acclimatization intensity must be a number');
                }
            }else{
                triggerPayload.acclimatizationIntensity = 0;
            }



            if(args.hasOwnProperty('preFocusDelay')){
                if(typeof (args.preFocusDelay) === 'number' && !(args.preFocusDelay%1)){
                    if( args.preFocusDelay <= 1000 && args.preFocusDelay >=0){
                        triggerPayload.preFocusDelay = args.preFocusDelay;
                    }else{
                        allOk = false;
                        errorMessages.push('Pre-focus delay must be between 0 and 10 - '+args.preFocusDelay+' given');
                    }
                }else{
                    allOk = false;
                    errorMessages.push('Pre-focus delay must be an integer');
                }
            }else{
                triggerPayload.preFocusDelay = 0;
            }


            if(args.hasOwnProperty('captureMode')){
                if(typeof (args.captureMode) === 'string'){
                    let acceptable = ['dslr', 'mv', 'video'];
                    if(acceptable.includes(args.captureMode)){
                        triggerPayload.captureMode = args.captureMode;
                    }else{
                        allOk = false;
                        errorMessages.push('Capture mode must be either "dslr", "mv" or "video" - "'+args.captureMode+'" given');
                    }
                }else{
                    allOk = false;
                    errorMessages.push('Capture mode must be a string');
                }
            }else{
                triggerPayload.captureMode = 'dslr';
            }


            if(allOk){
                this.socket.emit('api-trigger',triggerPayload,(response)=>{
                    if (response.status) {
                        resolve();
                    } else {
                        reject(response.errors);
                    }
                });
            }else{
                reject(errorMessages);
            }
        });
    }

    testFlash(){
        return new Promise((resolve, reject)=>{
            console.log("Test flash...");
            this.socket.emit('api-test-flash', (response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.errors);
                }
            });
        });
    }

    continuousCapture(args){
        return new Promise((resolve,reject)=>{

            if(typeof args.fps === 'undefined'){
                args.fps = 100;
            }
            if(typeof args.flashLag === 'undefined'){
                args.flashLag = 0;
            }
            if(typeof args.triggerDuration === 'undefined'){
                args.triggerDuration = 5;
            }
            this.socket.emit('api-continuous-sequence-start',args,(response)=>{
                if (response.status === true){
                    resolve();
                }
                else{
                    reject(response.errors);
                }
            })
        });

    }

    stopContinuousCapture(){
        return new Promise((resolve,reject)=>{
            this.socket.emit('api-continuous-sequence-stop',(response)=>{
                if (response.status === true){
                    resolve();
                }
                else{
                    reject(response.errors);
                }
            })
        })
    }

    // noinspection JSUnusedGlobalSymbols
    writeEEPROMAddresses(){
        return new Promise((resolve,reject)=>{
            this.socket.emit('api-write-eeprom-addresses',(response)=>{
                if (response.status === true){
                    resolve();
                }
                else{
                    reject(response.errors);
                }
            });
        })
    }



    // noinspection JSUnusedGlobalSymbols
    setNodeChain(rigPattern,nodeString,nodeTypeId = 1){
        return new Promise((resolve,reject)=>{
            let rigData = {
                wiringSequence: nodeString,
                name: rigPattern,
                nodeModelID:nodeTypeId,
            };
            this.socket.emit('set-rig',rigData,(response)=>{
                if (response.status === true){
                    resolve();
                }
                else{
                    reject(response.errors);
                }
            });
        })
    }

    setCurrentSequencePoint(newSequencePoint){
        return new Promise((resolve, reject)=>{
            console.log("Setting sequence point:" + newSequencePoint);
            this.socket.emit("api-set-current-sequence-position", newSequencePoint, (response)=>{
                if (response.status === true){
                    resolve();
                }
                else{
                    reject(response.errors);
                }
            })
        });
    }

    configureChainModeGen1(duration_millis){
        this.useBackendTerminalCommand("ccm-dur " + duration_millis);
    }
    armChainModeGen1(brightness){
        setTimeout(()=>{
            if (brightness <= 100  && brightness > 0){
                this.useBackendTerminalCommand("acm " + brightness);
            }
            else {
                console.log("ERR: brightness must be between 0.15% and 100%")
            }
        }, 1000)
    }

    rewindChainModeGen1(){
        this.useBackendTerminalCommand("k-idv");
    }

    exitChainModeGen1(onExitComplete){
        this.useBackendTerminalCommand("o");
        this.useBackendTerminalCommand("r");
        setTimeout(()=>{
            this.useBackendTerminalCommand("o");
            this.useBackendTerminalCommand("r");
            setTimeout(()=>{
                this.useBackendTerminalCommand("i");
                onExitComplete();
            }, 1000)
        }, 1000)
    }


    // noinspection JSUnusedGlobalSymbols
    rewindSequencePoint(){
        return new Promise((resolve,reject)=>{
            this.socket.emit('rewind-sequence-point',(response)=>{
                //todo this does not conform to standardised esper ws syntax
                if(response){
                    resolve();
                }else{
                    reject(['Could not rewind sequence point']);
                }
            })
        });
    }

    setSequenceStagePlaybackCount(payload) {
        return new Promise((resolve, reject) => {
            this.socket.emit("api-set-stage-playback-count", payload, (response) => {
                if (response.status === true) {
                    // noinspection JSUnresolvedVariable
                    console.log(`Stage Repeats set to ${payload.playBackCount}, for ID: ${payload.id}`);
                    resolve();
                } else {
                    console.err("ERR: stage repeats not set.");
                    reject("No confirmation from ControlSuite");
                }
            });
        });
    }

    tellMeWhenItsDone(){
        return new Promise((resolve, reject)=>{
            console.log("### WAITING FOR CONTROLLERBOX EMPTY ###");
            let maxStackSize = 3;
            let progress_perc = 0;
            let progressPoller;
            progressPoller = setInterval(()=>{
                console.log("### REQUESTING STACK SIZE ###")
                this.socket.emit("api-get-controller-box-stack-size", (response)=>{
                    console.log("## RESPONSE ##")
                    if (response.hasOwnProperty("stackSize")){
                        if (typeof response.stackSize === "number"){
                            if (response.stackSize > maxStackSize){
                                maxStackSize = response.stackSize;
                            }
                            console.log("## STACK_SIZE: " + response.stackSize + " ##");
                            progress_perc = (response.stackSize / maxStackSize) * 100;
                            console.log("## PROGRESS: " + Math.round(progress_perc) + "##");
                            if (response.stackSize === 0){
                                console.log("### EMPTY ###");
                                clearInterval(progressPoller);
                                resolve();
                            }
                        }
                    }
                })
            }, 500);

        });
    }

    wait(millisToWait = 0){
        return new Promise(((resolve) => {
            let numDots = 10;
            process.stdout.write("waiting");
            for (let i = 0; i< numDots; i++){
                setTimeout(()=>{
                        process.stdout.write(".");
                    },
                    (millisToWait/numDots)*i);
            }
            setTimeout(()=>{
                    resolve();
                    console.log();
                },
                millisToWait);
        }));
    }

    loadEEPROMIDs(){
        return new Promise((resolve)=>{
            this.socket.emit("load-eep-addr");
            resolve();
        })
    }

    useBackendTerminalCommand(termCommand, onCommanded){
        this.socket.emit("api-terminal", termCommand, (respobject)=>{
            // noinspection JSUnresolvedVariable
            // if (respobject.messageBack === "ACK"){
            console.log("running command: " + termCommand);
            if (onCommanded){
                onCommanded();
            }
            // }
        });
    }
    useBackendTerminalCommandPromise(termCommand){
       return new Promise((resolve)=>{
           this.useBackendTerminalCommand(termCommand,resolve);
       })
    }

    goChainMode(){
        return this.useBackendTerminalCommandPromise(`m 3`);
    }
    goSequencerMode(){
        return this.useBackendTerminalCommandPromise(`m 1`);
    }
    getTriggerBoxes(){
        return new Promise((resolve,reject)=>{
            this.socket.emit("get-triggerboxes",(response)=>{
                if(response.status){
                    this.triggerboxes = response.msg;
                    resolve(response.msg);
                }else{
                    reject(response.msg);
                }
            })
        })
    }

    saveAllTriggerboxes(){
        return new Promise((resolve,reject)=>{
            this.socket.emit("save-all-triggerboxes",(response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.msg);
                }
            })
        })
    }
    restartAllTriggerboxes(){
        return new Promise((resolve,reject)=>{
            this.socket.emit("restart-all-triggerboxes",(response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.msg);
                }
            })
        })
    }
    factoryResetAllTriggerboxes(){
        console.log("resetting called");
        return new Promise((resolve,reject)=>{
            this.socket.emit("factory-reset-all-triggerboxes",(response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.msg);
                }
            })
        })
    }
    setShutterEnableAll(enableArray){
        return new Promise((resolve,reject)=>{
            this.socket.emit("set-shutter-enable-all-triggerboxes",enableArray,(response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.msg);
                }
            })
        })
    }
    setFocusEnableAll(enableArray){
        return new Promise((resolve,reject)=>{
            this.socket.emit("set-focus-enable-all-triggerboxes",enableArray,(response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.msg);
                }
            })
        })
    }
    setDelayAll(delayArray){
        return new Promise((resolve,reject)=>{
            this.socket.emit("set-delay-all-triggerboxes",delayArray,(response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.msg);
                }
            })
        })
    }
    setOutputModeAll(enableArray){
        return new Promise((resolve,reject)=>{
            this.socket.emit("set-output-mode-all-triggerboxes",enableArray,(response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.msg);
                }
            })
        })
    }
    setInputModeAll(modeInt){
        return new Promise((resolve,reject)=>{
            this.socket.emit("set-input-mode-all-triggerboxes",modeInt,(response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.msg);
                }
            })
        })
    }

    saveTriggerbox(id){
        return new Promise((resolve,reject)=>{
            this.socket.emit("save-triggerbox",id,(response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.msg);
                }
            })
        })
    }
    restartTriggerbox(id){
        return new Promise((resolve,reject)=>{
            this.socket.emit("restart-triggerbox",id,(response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.msg);
                }
            })
        })
    }
    factoryResetTriggerbox(id){
        return new Promise((resolve,reject)=>{
            this.socket.emit("factory-reset-triggerbox",id,(response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.msg);
                }
            })
        })
    }
    setShutterEnableTriggerbox(id,enableArray){
        return new Promise((resolve,reject)=>{
            this.socket.emit("set-shutter-enable-triggerbox",enableArray,(response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.msg);
                }
            })
        })
    }
    setFocusEnableTriggerbox(id,enableArray){
        return new Promise((resolve,reject)=>{
            this.socket.emit("set-focus-enable-triggerbox",id,enableArray,(response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.msg);
                }
            })
        })
    }
    setDelayTriggerbox(id,delayArray){
        return new Promise((resolve,reject)=>{
            this.socket.emit("set-delay-triggerbox",id,delayArray,(response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.msg);
                }
            })
        })
    }
    setOutputModeTriggerbox(id,outputArray){
        return new Promise((resolve,reject)=>{
            this.socket.emit("set-output-mode-triggerbox",id,outputArray,(response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.msg);
                }
            })
        })
    }
    setInputModeTriggerbox(id,modeInt){
        return new Promise((resolve,reject)=>{
            this.socket.emit("set-input-mode-triggerbox",id,modeInt,(response)=>{
                if(response.status){
                    resolve();
                }else{
                    reject(response.msg);
                }
            })
        })
    }

    testBackendTerminalCommand(testCommand, onTested){
        this.socket.emit("api-terminal-test-command", testCommand, (respobject)=>{
            //console.log(respobject);
            onTested(respobject);
            // if (respobject.validCommands){
            //     for (let vc of respobject.validCommands){
            //         console.log(vc);
            //     }
            // }
        });
    }


    describeErrors(errorsInput){
        //console.log(errorsInput);
        if (errorsInput === ""){

        }
        else if (Array.isArray(errorsInput)){
            console.table(errorsInput);
        }
        else if (typeof errorsInput === "string"){
            console.log("ERROR" + errorsInput);
        }
    }


    disconnect(){
        this.socket.close();
    }

    syncReadFPS(){
        return new Promise((resolve, reject)=>{
            let respTimeout = setTimeout(()=>{
                console.log("No controlSuiteResponse");
                reject();
            }, 1000);
            this.socket.emit("api-read-sync-FPS", (respObj)=>{
                clearTimeout(respTimeout);
                if (respObj.status === true){
                    if (respObj.hasOwnProperty("fpsRead")){
                        resolve(respObj.fpsRead);
                    }
                    else{
                        reject("No FPS data resp error.")
                    }
                }
            })
        })
    }

    __syncCaptureFunctions = {
        syncCaptureStop(){

        },

        syncCaptureContinuous(inputMask, outputMask, flashLag, flashesPerInput, outputDuration) {
            this.syncCaptureNumFrames(inputMask, outputMask, flashLag, flashesPerInput, outputDuration,0);
        },

        syncCaptureNumFrames(inputMask, outputMask, flashLag, flashesPerInput,  outputDuration, numFrames){
            let argObj = {}; for (let arg of arguments){ argObj.arg = arg; }
            //this.syncCaptureNumFrames(argObj);
        },

        syncCaptureForMillis(inputMask, outputMask, flashLag, flashesPerInput,  outputDuration, millisDur){
            let argObj = {}; for (let arg of arguments){ argObj.arg = arg; }
            //this.syncCaptureForMillis(argObj);
        }
    }

};

process.on('unhandledRejection', error => {
    console.log('unhandledRejection', error);
});
