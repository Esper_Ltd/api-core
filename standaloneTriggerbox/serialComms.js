const SerialBox = require("./serialBox.js");
//stuff from config
let cbRevisions = ["!R6ControllerBox%","!R7ControllerBox%"];


//placeholders for this module

//array for serialBox objects - need them to persist for pass-by-reference
let allSerialDevices = [];

//create a message to send for each of the detection
let triggerIDCommand = "*calling_ESPER_triggerBox\n";
let controllerIDCommand = SerialBox.generateSendBytes(["W"]);

let handleControllerboxFunction = null;
let handleTriggerboxFunction = null;
let theProgressFunction = null;





let start = async()=>{
    return new Promise((resolve,reject)=>{

        if( handleTriggerboxFunction === null ||
            handleControllerboxFunction === null ||
            theProgressFunction === null
        ){
            reject("handler functions not present, make sure to call registerControllerBoxHandler() and registerTriggerBoxHandler()");
        }
        //call the static member function that asynchronously returns an array of strings
        SerialBox.findPortsWithAnFTDIDevice().then((ports)=>{

            let deviceStartupPromises = [];

            for(let i=0;i<ports.length;i++){
                allSerialDevices[i] = new SerialBox(ports[i],true);

                allSerialDevices[i].registerSpecialisation(
                    "triggerbox",
                    115200,
                    ()=>{
                        console.log("STOPPING ID ROUTINE FOR TRIGGERBOX");
                        setTimeout(()=>{allSerialDevices[i].sendPlaintext("^","ack triggerbox");},0)

                    },
                    triggerIDCommand,
                    ["TriggerBox:[box_id]"],true);

                deviceStartupPromises.push(allSerialDevices[i].startup().then((isDetected)=>{
                    console.log("startup promise returned===============");
                    if(isDetected){
                        console.log("running switch on serial device...");
                        switch(allSerialDevices[i].boxType){
                            case "triggerbox":
                                handleTriggerboxFunction(allSerialDevices[i]);
                                console.log("IDENTIFIED TRIGGERBOX");
                                break;
                            case "ControllerBox":
                                allSerialDevices[i].attachProgressFunction(theProgressFunction);
                                handleControllerboxFunction(allSerialDevices[i]);
                                break;
                            default:
                                console.log("identified as something else - "+allSerialDevices[i].boxType);
                        }
                    }else{
                        console.log("serial device not identifed");
                    }
                    return Promise.resolve();
                }));
                Promise.all(deviceStartupPromises).then(resolve).catch(reject);
            }
        });

    })
}


module.exports = {
    start:start,
    registerControllerBoxHandler:(theFunction)=>{handleControllerboxFunction=theFunction;},
    registerTriggerBoxHandler:(theFunction)=>{handleTriggerboxFunction=theFunction;},
    registerProgressFunction:(theFunction)=>{theProgressFunction=theFunction;}
}