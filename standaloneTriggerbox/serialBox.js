const Helper = require("./helper.js");
const { SerialPort } = require('serialport');
const { ByteLengthParser } = require('@serialport/parser-byte-length');


function confirmSpecialisationArgs(specialisationName,baudRate,onIdentified, enumerationCommand, identificationResponses){
    if(
        typeof specialisationName === "string" &&
        typeof baudRate === "number" &&
        (Helper.typist.checkIsByteArray(enumerationCommand,true) || typeof enumerationCommand === "string") &&
        typeof identificationResponses ==="object"
    ){
        if(Object.prototype.hasOwnProperty.call(identificationResponses,"length")){
            //all params passed
            if(
                specialisationName.length > 0 &&
                identificationResponses.length > 0
            ){
                //think everything is good from an input POV
                return 0;
            }else{
                return "params all passed but one or more are zero length";
            }
        }else{
            return "identificationResponses param is not an array";
        }
    }else{
        return "params passed to registerSpecialisation incomplete";
    }
}



class SerialBox {
    constructor(port, verbose=false){
        this.port = port;
        this.verbose=verbose;

        this.serialPort = new SerialPort({baudRate: 115200, path: port},
            (err) => {
                if (err){
                    console.error(err);
                    this.handlePortConnectionError(err);
                } else{

                    this.serialByteGetter = this.serialPort.pipe(new ByteLengthParser({
                        length: 1
                    }));
                    this.serialByteGetter.on('data', (data) => {
                        let byteToBufferBack = data.readUInt8();
                        this.bytesReceived(byteToBufferBack);
                    });

                    this.serialPort.on("close", (err) => {
                        if (err){
                            Helper.print("COM Close Error: " + err);
                        } else{
                            Helper.printInYellow("Closed com port...");
                        }
                    });
                }
            }
        );
    }
    attachProgressFunction(loadingFunct){
        if(typeof loadingFunct === "function"){
            this.progressFunct = loadingFunct;
        }
    }
    progressFunct=()=>{};
    progressTimeout=null;
    plaintext=false;
    active = false;
    progressInterval=null;
    baudRate=115200;
    port="";
    verbose=false;
    forceContinue=false;
    specialisations=[]
    specialisation='';
    boxType='';
    bufferIn=[];
    currentCommand=null;
    commandTimeout=null;
    commandStack = [];
    commandStackSize=0;
    stackSizeForProgressReport=11;
    stackProgress=0;        //0 to 100
    retriesAllowed = 5;
    timeToLiveMillis = 500;
    retryBytes = [];
    numFailedCommands = 0;
    totalStackRetries = 0;
    cancelThreshold = 3;
    lastMessageMillis = -1;
    nextCommandMillis = 0;  //this was set to 5ms - you get js related benefits to setTimout(0) - executes when stack is next empty
    maxCommandStackSize = 0;
    commandCounter = 0;
    registerSpecialisation(specialisationName,baudRate, onIdentified, enumerationCommand, identificationResponses,plaintext){
        //ascertain that params passed are good
        let response =confirmSpecialisationArgs(specialisationName,baudRate, onIdentified, enumerationCommand, identificationResponses);
        if(response !== 0){throw response;}

        //we can continue
        this.specialisations.push({specialisationName,baudRate,onIdentified, enumerationCommand, identificationResponses,plaintext:!!plaintext});
    }


    handlePortConnectionError(err){
        console.log(`port connection error ${err}`);
        if(this.serialPort.isOpen){
            this.serialPort.close(function (err) {
                console.log('port closed', err);
            });
        }
        this.active = false;
    }

    startup(){
        return new Promise((resolve)=>{
            this.forceContinue = true;
            this.retriesAllowed = 1;
            for(let i=0;i<this.specialisations.length;i++){
                this.plaintext = this.specialisations[i].plaintext; //stop the erroring out on startup - this gets set accordingly on the successCallback
                this.addCommand(
                    this.specialisations[i].enumerationCommand,
                    `Checking if device is ${this.specialisations[i].specialisationName} with plaintext:${!!this.specialisations[i].plaintext}`,
                    (response) => {
                        let respStr = Helper.byteArrayToString(response);
                        for (let rIn = 0; rIn < this.specialisations[i].identificationResponses.length; rIn++){
                            if (respStr === this.specialisations[i].identificationResponses[rIn]){
                                this.specialisation = respStr;
                                return 0;
                            }
                        }
                        return 1;
                        }, -1,
            ()=>{
                            this.plaintext = this.specialisations[i].plaintext;
                            this.serialPort.update({baudRate:this.specialisations[i].baudRate});
                            this.boxType = this.specialisations[i].specialisationName;
                            this.plaintext=this.specialisations[i].plaintext;
                            this.specialisations[i].onIdentified();
                            resolve(true);
                            this.identifiedCallback();
                        },
        ()=>{
                        this.identifcationFailureCallback();
                        if(i === (this.specialisations.length -1)){
                            resolve(false);
                        }
                    },
            ()=>{
                        if(this.baudRate !== this.specialisations[i].baudRate){
                            this.serialPort.update({baudRate:this.specialisations[i].baudRate});
                        }
                    },);

            }
            this.nextCommand();
        })

    }
    identifiedCallback(){
        console.log(`serialBox identifed callback ran with ${this.boxType}`);
        this.active = true;
        this.forceContinue=false;
        this.numFailedCommands = 0;
        this.commandStack = [];
        this.totalStackRetries = 0;
        this.retryBytes = [];
        this.retriesAllowed=5;
    }
    identifcationFailureCallback(){
        console.log("FAILED TO IDENTIFY");
    }
    bytesReceived(byteBack){

        if (typeof byteBack !== "undefined" && typeof byteBack === "number"){
            this.bufferIn.push(byteBack); // deal with address zero / no reply expected
        } else if (typeof byteBack !== "undefined" && typeof byteBack !== "number"){
            Helper.printInRed("byteBack reccd not type num:" + byteBack);
        }
        if (this.currentCommand !== null){
            if (typeof this.currentCommand.responseAssessor === 'function'){
                if (this.currentCommand.rle === 0){
                    clearTimeout(this.commandTimeout);            // Clear the timeout
                    // noinspection JSUnresolvedFunction
                    this.serialPort.drain((err) => {
                        if (err){
                            Helper.printInRed("COM-Drain Error:" + err);
                        }
                        else{
                            if(this.currentCommand){
                                if (this.currentCommand.bytesOut){
                                    if (this.currentCommand.bytesOut.length > 0){
                                        Helper.printInYellow("drain");
                                    }
                                }
                                if (typeof this.currentCommand.success === "function"){
                                    try{
                                        this.currentCommand.success();
                                    } catch (err){
                                        Helper.printInRed("CAUGHT: " + err);
                                    }
                                }
                                setTimeout(()=>{this.commandDone();}, 2);
                            }else{
                                console.log("currentCommand Falsy - "+this.bufferIn.join(""));
                                console.log("byte was :"+String.fromCharCode(byteBack));
                            }
                        }
                    });
                }
                else if (this.currentCommand.rle === -2){
                    clearTimeout(this.commandTimeout);
                    if (typeof this.currentCommand.success === "function"){
                        this.currentCommand.success();
                    } else{
                        Helper.printInRed("Current command has empty success callback");
                    }
                    this.commandDone();
                }
                else{
                    let responseAssessmentCode = this.currentCommand.responseAssessor(this.bufferIn, this.currentCommand.rle);
                    if (responseAssessmentCode === 0){
                        /* Command is good  */
                        clearTimeout(this.commandTimeout);         // Clear the timeout
                        let bytesInAsTriSpaced = Helper.uniformSpacing(this.bufferIn);
                        let millisBack = new Date().getUTCMilliseconds();
                        let millisResp = -1;
                        try{
                            millisResp = millisBack - this.currentCommand.millisSendTime;
                        } catch(err){Helper.printInRed("MillisCalcError:" + err);}
                        Helper.print(`${this.serialPort.path} <<< {${bytesInAsTriSpaced}} (${millisResp}mS)`);
                        this.currentCommand.success(this.bufferIn, this.currentCommand.bytesOut);     // Run the success function
                        this.commandDone();                        // sort out the next in the stack
                    } else if (responseAssessmentCode === 1){
                        // still buffering in.
                        if(byteBack === 10 && this.plaintext){
                            //got a newline and didnt recognise the buffer, clear it for the next line??

                            this.bufferIn =[];
                        }
                    } else if (responseAssessmentCode === 2){
                        // Command is not what we expected.
                        this.commandNotWorked();
                    } else if (responseAssessmentCode === 3){
                        // Command is not what we expected but we dont care - clear input buffer and wait - triggerbox discovery

                        Helper.printInYellow("CLEARING INCOMING BUFFER: "+Helper.byteArrayToString(this.bufferIn));
                        this.bufferIn = []
                    }
                }
            }
            else{
                Helper.printInYellow("NO RESPONSE ASSESSOR.");
            }
        }
        else{
            if(this.plaintext){
                if(byteBack === 10){
                    this.bufferIn = []; //newline, clear buffer
                }
            }else{
                Helper.printInRed("Unexpected ByteBack:" + byteBack);
                this.forceClearStack(false);
            }
        }
    }
    commandDone(){
    console.log("command done");
        clearTimeout(this.commandTimeout);
        if(this.currentCommand){
            this.stackProgress = Math.floor((this.currentCommand.stackPosition / this.commandStackSize)*100);
        }else{
            this.stackProgress = 0;
        }
        this.currentCommand = null;
        this.bufferIn = [];
        setTimeout(() => {
            this.nextCommand();
        }, this.nextCommandMillis);
    }
    nextCommand(){
        if (this.currentCommand === null){
            if (this.numFailedCommands >= this.cancelThreshold){
                this.forceClearStack(true);
                this.commandStackComplete();
            }
            if (this.commandStack.length > 0){
                this.currentCommand = this.commandStack.shift();
                this.bufferIn = [];
                this.sendCurrentCommand();
                this.commandCounter++;
                if (this.commandStack.length > this.maxCommandStackSize){
                    this.maxCommandStackSize = this.commandStack.length;
                }
            } else{
                this.commandStackComplete();
                this.maxCommandStackSize = 0;
            }
        }
    }

    commandNotWorked(){
        let uniSpaced = "{" + Helper.uniformSpacing(this.bufferIn) + "}";
        if (this.serialPort){
            Helper.printInYellow("! <<< " + uniSpaced);
            if(this.plaintext){
                console.log(Helper.byteArrayToString(this.bufferIn));
            }
            Helper.printInYellow(" ### Command not verified ###");
        }
        clearTimeout(this.commandTimeout);
        if (this.currentCommand){
            if (this.currentCommand.retries !== null){
                this.currentCommand.retries++;
            }
            setTimeout(() => {
                if (this.currentCommand !== null){
                    if (this.currentCommand.retries <= this.retriesAllowed){
                        this.bufferIn = [];
                        this.sendCurrentCommand();
                        this.retryBytes.push(this.currentCommand.bytesOut);
                    } else{
                        console.error("Command failed: " + this.currentCommand.commandDesc);
                        this.numFailedCommands++;
                        this.currentCommand.failure(this.bufferIn);
                        this.commandDone();
                        this.forceClearStack(false);
                    }
                }
            }, 10);
        }
    }

    sendCurrentCommand(){
        if (this.serialPort){
            Helper.println();
            Helper.printInBlue(this.currentCommand.commandDesc);
            let bytesOutAsTriSpaced = Helper.uniformSpacing(this.currentCommand.bytesOut);
            if (this.currentCommand.bytesOut.length > 0){
                Helper.print(`${this.serialPort.path} >>> {${bytesOutAsTriSpaced}}`);
                if(typeof this.currentCommand.preSend === "function"){
                    this.currentCommand.preSend();
                }
                this.serialPort.write(this.currentCommand.bytesOut);
                this.currentCommand.millisSendTime = new Date().getUTCMilliseconds();
            }
            this.lastMessageMillis = Date.now();
            this.commandTimeout = setTimeout(() => {
                this.commandNotWorked();
            }, this.timeToLiveMillis);
            this.bytesReceived();   //this deals with no reply expected commands
        }
        else{
            // If running to here, likely using the sim or just clicking around the GUI with no ControllerBox
            setTimeout(() => {
                try{
                    this.currentCommand = null;
                    this.nextCommand();
                }catch(err){
                    console.log(err);
                }
            }, 50);
        }
    }
    forceClearStack(){
        if (!this.forceContinue){
            let numSkippedCommands = this.commandStack.length;
            console.error(`Skipping ${numSkippedCommands} commands...`);
            this.numFailedCommands = 0;
            this.commandStackSize=0;
            this.commandStack = [];
            this.currentCommand = null;
            this.totalStackRetries = 0;
            this.retryBytes = [];
        }
    }

    commandStackComplete(){
        Helper.printInBlue("Sent " + this.commandCounter + " commands");
        //check totalStackRetries and decide if the user needs to be told about them
        this.commandCounter = 0;
        this.currentCommand = null;
        this.commandStackSize=0;
        //reset totalStackRetries and retryBytes
        this.totalStackRetries = 0;
        this.retryBytes = [];
    }

    addCommand(bytes, messageDescription, responseAssessor, replyLengthExpect, successCallback, failureCallback,preSend=()=>{}){
        let argsValid = (
            Helper.typist.checkIsDescription(messageDescription) &&
            Helper.typist.checkIsFunction(responseAssessor) &&
            Helper.typist.checkIsInt(replyLengthExpect) &&
            Helper.typist.checkIsFunction(successCallback) &&
            Helper.typist.checkIsFunction(failureCallback));

        if(!this.plaintext){
            argsValid = argsValid && Helper.typist.checkIsByteArray(bytes)
        }
        if (!argsValid){
            console.error("ERR: addCommand args not verified");
            return;
        }

        this.commandStackSize+=1;
        this.commandStack.push({
            bytesOut: bytes,
            commandDesc: messageDescription,
            responseAssessor: responseAssessor,
            rle: replyLengthExpect,
            timeToLiveMillis: this.timeToLiveMillis,
            success: successCallback,
            failure: failureCallback,
            preSend,
            retries: 0,
            stackPosition:this.commandStackSize
        });
        this.checkForProgressReporting();
        this.nextCommand();
    }

    checkForProgressReporting(){
        if(this.progressTimeout === null){
            if(this.commandStackSize > this.stackSizeForProgressReport){
                this.progressTimeout = setTimeout(
                ()=>{
                    this.progressTimeout = null
                    if(this.commandStackSize > this.stackSizeForProgressReport) {
                        if (this.progressInterval === null) {
                            this.progressInterval = setInterval(() => {
                                if (this.commandStack.length === 0) {
                                    this.progressFunct("done", false, 100);
                                    clearInterval(this.progressInterval);
                                    this.progressInterval = null;
                                } else {
                                    this.progressFunct(this.commandStack[0].commandDesc, true, this.stackProgress)
                                }
                            }, 500);
                        }
                    }
                },1000);
            }
        }
    }


    /**
     *
     * @param {number[]} controlBytes
     * @param {string} messageDescription
     * @param {function} responseAssessor
     * @param {function} onSuccess
     * @param {function} onReplyFailure
     */
    sendControlMessage(controlBytes, messageDescription, responseAssessor, onSuccess, onReplyFailure = ()=>{}){
        let cmd = this.generateControlMessage(controlBytes, messageDescription, responseAssessor, onSuccess, onReplyFailure )
        if(cmd !== 1){
            this.addCommand(cmd.cmd,cmd.messageDescription,cmd.responseAssessor,cmd.replyLength,cmd.onSuccess,cmd.onFail,()=>{});
        }
    }

    sendModbusMessage(line, modMsg, respAssess, onReply, msgDescription, replyLengthIn = -1, onReplyFail){
        let cmd = this.generateModbusMessage(line, modMsg, respAssess, onReply, msgDescription, replyLengthIn , onReplyFail);
        if(cmd !== 1){
            this.addCommand(cmd.cmd,cmd.msgDescription,cmd.respAssess,cmd.replyLength,cmd.onSuccess,cmd.onFail,()=>{});
        }

    }

    sendPlaintext(msg,msgDescription,respAssess=()=>{return 0},onReply=()=>{}, replyLengthIn=0, onReplyFail =()=>{}){
        this.addCommand(`${msg}\n`,msgDescription,respAssess,replyLengthIn,onReply,onReplyFail,()=>{});
    }

    generateControlMessage(controlBytes, messageDescription, responseAssessor, onSuccess, onReplyFailure = ()=>{}){
        let argsValid = Helper.typist.checkIsByteArray(controlBytes) &&
            Helper.typist.checkIsDescription(messageDescription) &&
            Helper.typist.checkIsFunction(responseAssessor) &&
            Helper.typist.checkIsFunction(onSuccess) &&
            Helper.typist.checkIsFunction(onReplyFailure);
        if (!argsValid){
            Helper.printInRed("ERR: args not verified");
            onReplyFailure(false);
            return 1;
        }else{
            let cmd = SerialBox.generateSendBytes(controlBytes);
            return  {
                cmd:cmd,
                messageDescription:messageDescription,
                responseAssessor:responseAssessor,
                replyLength:-1,
                onSuccess:onSuccess,
                onFail:onReplyFailure
            };
        }
    }

    generateModbusMessage(line, modMsg, respAssess, onReply, msgDescription, replyLengthIn = -1, onReplyFail){
        if (typeof onReplyFail !== "function"){
            // noinspection AssignmentToFunctionParameterJS
            onReplyFail = ()=>{};
        }

        let argsValid = (
            Helper.typist.checkIsInt(line) &&
            Helper.typist.checkIsByteArray(modMsg) &&
            Helper.typist.checkIsFunction(respAssess) &&
            Helper.typist.checkIsFunction(onReply) &&
            Helper.typist.checkIsDescription(msgDescription) &&
            Helper.typist.checkIsInt(replyLengthIn));
        if (!argsValid){
            Helper.printInRed("ERR: args not verified");
            return 1;
        }else{
            let replyLength;
            if (modMsg[0] === 0 && replyLengthIn === -1){
                replyLength = 0;
            } else if (replyLengthIn === -1){
                replyLength = modMsg.length;
            } else{
                replyLength = replyLengthIn;
            }
            let byteContentOut = [];
            byteContentOut[0] = '@'.charCodeAt(0);
            /// skip [1];
            byteContentOut[2] = line;
            byteContentOut[3] = modMsg.length;
            byteContentOut[4] = replyLength;
            for (let i = 0; i < modMsg.length; i++){
                byteContentOut[i + 5] = modMsg[i];
            }
            byteContentOut[1] = byteContentOut.length;

            return {
                cmd:byteContentOut,
                msgDescription,
                respAssess,
                replyLength,
                onSuccess:onReply,
                onFail:onReplyFail
            }
        }

    }


    static stringToBytes(controlPayload){
        let sendBytes = [];
        for (let b of controlPayload){
            if (typeof b === "string"){
                b = b.charCodeAt(0);
            }
            sendBytes = sendBytes.concat(b);
        }
        return sendBytes;
    }
    static generateSendBytes(controlPayload){
        let sendBytes = [];
        sendBytes.push("!".charCodeAt(0));
        sendBytes.push(1); // will be over written
        for (let b of controlPayload){
            if (typeof b === "string"){
                b = b.charCodeAt(0);
            }
            sendBytes = sendBytes.concat(b);
        }
        sendBytes[1] = sendBytes.length;
        return sendBytes;
    }

    static findPortsWithAnFTDIDevice(){
        return new Promise((resolve,reject)=>{
            SerialPort.list()
                .then((ports)=>{
                    let devices = [];
                    for(let i=0;i<ports.length;i++){
                        try{
                            if(ports[i].manufacturer === "FTDI"){
                                console.log(`Found what could be an Esper serial device on port ${ports[i].path}`);
                                devices.push(ports[i].path);
                            }
                        }catch(e){
                            console.log("ports returned did not have property 'manufacturer'");
                        }

                    }
                    resolve(devices);
                }).catch(reject);
        });
    };

}


module.exports = SerialBox;