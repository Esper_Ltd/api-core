const helper = require('./helper');
function lineIncludes(resp,toMatch){
    if(typeof resp === "string"){
        if(resp.charCodeAt(resp.length -1) === 10 ){
            let line = helper.byteArrayToString(resp);  //think the conversion to string is happening before this function is called.... //todo
            if(resp.includes(toMatch)){
                return 0;   //happy
            }else{
                return 3;   //not happy
            }
        }else{
            return 1;   //not yet complete
        }
    }else{

    }
}

function isTenFourByteArray(byteArray){
    if(byteArray[byteArray.length -1] === 10){  //newline terminated
        let asString = helper.byteArrayToString(byteArray);
        if(asString.includes("tenFour")){
            return 0;
        }else{
            return 3;   //3 means message not right, but clear the recv buffer and keep going - dont error our
        }
    }else{
        return 1;
    }
}
function checkIsMVResponseAssessor(byteArray){
    if(byteArray[byteArray.length -1] === 10){  //newline terminated
        let asString = helper.byteArrayToString(byteArray);
        if(asString.includes("fourTen: Bad command")){
            return 0;
        }else if(asString.includes("InputMode")){
            return 0;
        }else{
            return 3;   //3 means message not right, but clear the recv buffer and keep going - dont error our
        }
    }else{
        return 1;
    }
}

function getArrayBetweenBrackets(toSplit){
    let start = toSplit.indexOf("[") +1;
    let finish = toSplit.indexOf("]") -1;
    return toSplit.substring(start,finish).split(' ');
}



class Triggerbox {
    constructor(SerialBoxObject,id) {
        this.serial = SerialBoxObject;
        this.id=id;
        this.isMV=false;    //placeholder for now - confirm by checking instruction set it responds to
        this.stopIdentificationMode()
            .then(()=>{return this.confirmState();})
            .then(()=>{
            console.log("got state;");
            console.log(`shutterEnable:${this.shutterEnable}`);
            console.log(`focusEnable:${this.focusEnable}`);
            console.log(`delays:${this.delays}`);
            console.log(`outputMode:${this.outputMode}`);
            console.log(`inputMode:${this.inputMode}`);
        }).catch((e)=>{
            console.log(`there was an error in getting state of triggerbox - ${e}`);
        });
    }
    shutterEnable = [0,0,0,0,0,0];
    focusEnable = [0,0,0,0,0,0];
    delays = [0,0,0,0,0,0];
    outputMode = [0,0,0,0,0,0];
    inputMode=0;

    reportState(){
        return {
            shutterEnable:this.shutterEnable,
            focusEnable:this.shutterEnable,
            delays:this.delays,
            outputMode:this.outputMode,
            inputMode:this.inputMode
        }
    }

    confirmState(){
        console.log("Confirming triggerbox state");
        return this.checkIfIsMV()
            .then(()=>{
                return this.getInputModeState();
            })
            .then(()=>{
                return this.getDelaysState();
            })
            .then(()=>{
                return this.getShutterEnableState();
            })

            .then(()=>{
                return this.getOutputModeState();
            })
            .then(()=>{
                return this.getFocusEnableState();
            })
    }
    saveSettings(){
        return new Promise((resolve,reject)=>{
            this.serial.sendPlaintext("O","saving settings to EEPROM",isTenFourByteArray,resolve,7,reject);
        })
    }
    restart(){
        return new Promise((resolve,reject)=>{
            this.serial.sendPlaintext("R","restarting triggerbox",isTenFourByteArray,resolve,7,reject);
        })
    }
    factoryReset(){
        return new Promise((resolve,reject)=>{
            this.serial.sendPlaintext("q","factory resetting triggerbox",isTenFourByteArray,resolve,7,reject);
        })
    }
    checkIfIsMV(){
        return new Promise((resolve,reject)=>{
            this.serial.sendPlaintext("/I","checking if is MV triggerbox",checkIsMVResponseAssessor,(reply)=>{
                let asString = helper.byteArrayToString(reply);
                this.isMV = asString.includes("InputMode")
                if(this.isMV){
                    helper.printInGreen("DETECTED MV TRIGGER");
                }else{
                    helper.printInGreen("DETECTED DSLR TRIGGER");
                }
                resolve();

            },7,reject);
        })
    }
    sendStopIDModeMessage(){
        return new Promise((resolve,reject)=>{
            this.serial.sendPlaintext("^","stop identification mode",isTenFourByteArray,resolve,7,reject);
        })
    }
    stopIdentificationMode(){
        return new Promise((resolve,reject)=>{
            let allInstructions = [];
            allInstructions.push(this.sendStopIDModeMessage());
            allInstructions.push(this.sendStopIDModeMessage());
            Promise.all(allInstructions).then(()=>{setTimeout(resolve,1000)}).catch(reject);
        })
    }
    getShutterEnableState(){
        return new Promise((resolve,reject)=>{
            this.serial.sendPlaintext('G',"getting shutterEnable state",(resp)=>{
                let line = helper.byteArrayToString(resp);
                return lineIncludes(line,"ShutterArray")
            },(response)=>{
                let line = helper.byteArrayToString(response);
                let shutters = getArrayBetweenBrackets(line);
                let ok =true;
                if(shutters.length === 6){
                    for(let i=0;i<6;i++){
                        if(shutters[i] === "DIS"){
                            this.shutterEnable[i]=0;
                        }else if(shutters[i] === "EN"){
                            this.shutterEnable[i]=1;
                        }else{
                            ok=false;
                        }
                    }
                }else{
                    ok=false;
                }
                if(ok){
                    resolve();
                }else{
                    reject(`could not understand response from triggerbox - ${helper.byteArrayToString(response)}`);
                }

            },-1,reject);
        })
    }
    setShutterEnable(enableBoolArray){
        return new Promise((resolve,reject)=>{
            if(enableBoolArray.length === 6){
                let toSend ='s';
                let ok = true;
                for(let i=0;i<6;i++){
                    if(typeof enableBoolArray[i] ==="boolean"){
                        if(!!enableBoolArray[i]){
                            toSend+='1';
                        }else{
                            toSend+='0';
                        }
                    }else{
                        ok=false;
                    }
                }
                if(ok){
                    this.serial.sendPlaintext(toSend,"setting shutterEnable array",isTenFourByteArray,()=>{this.getShutterEnableState().then(resolve).catch(reject)},7,reject);
                }else{
                    reject("array passed did not contain booleans")
                }
            }else{
                reject("must pass all 6 states - one for each output");
            }
        })
    }
    getFocusEnableState(){
        return new Promise((resolve,reject)=>{
            this.serial.sendPlaintext('V',"getting focusEnable state",(resp)=>{
                let line = helper.byteArrayToString(resp);
                return lineIncludes(line,"FocusArray")
            },(response)=>{
                let line = helper.byteArrayToString(response);
                let focuses = getArrayBetweenBrackets(line);
                let ok=true;
                if(focuses.length === 6){
                    for(let i=0;i<6;i++){
                        if(focuses[i] === "DIS"){
                            this.focusEnable[i]=0;
                        }else if(focuses[i] === "EN"){
                            this.focusEnable[i]=1;
                        }else{
                            ok=false;
                        }
                    }
                }else{
                    ok=false;
                }
                if(ok){
                    resolve();
                }else{
                    reject(`could not understand response from triggerbox - ${helper.byteArrayToString(response)}`);
                }

            },-1,reject);
        })
    }
    setFocusEnable(enableBoolArray){
        return new Promise((resolve,reject)=>{
            if(enableBoolArray.length === 6){
                let toSend ='f';
                let ok = true;
                for(let i=0;i<6;i++){
                    if(typeof enableBoolArray[i] ==="boolean"){
                        if(!!enableBoolArray[i]){
                            toSend+='1';
                        }else{
                            toSend+='0';
                        }
                    }else{
                        ok=false;
                    }
                }
                if(ok){
                    this.serial.sendPlaintext(toSend,"setting focusEnable array",isTenFourByteArray,()=>{this.getFocusEnableState().then(resolve).catch(reject)},7,reject);
                }else{
                    reject("array passed did not contain booleans")
                }
            }else{
                reject("must pass all 6 states - one for each output");
            }
        })
    }
    getDelaysState(){
        return new Promise((resolve,reject)=>{
            this.serial.sendPlaintext('D',"getting delay state",(resp)=>{
                let line = helper.byteArrayToString(resp);
                return lineIncludes(line,"Delays")
            },(response)=>{
                let line = helper.byteArrayToString(response);
                let delays = getArrayBetweenBrackets(line);
                if(delays.length === 6){
                    for(let i=0;i<6;i++){
                        this.delays[i] = parseInt(delays[i]);
                    }
                    resolve();
                }else{
                    reject(`could not understand response from triggerbox - ${helper.byteArrayToString(response)}`);
                }

            },-1,reject)
        })
    }
    setBulbTime(bulbTime){
        return new Promise((resolve,reject)=>{
            console.log("setting bulb time")
            return this.sendSerial(`/b ${bulbTime} ${bulbTime} ${bulbTime} ${bulbTime} ${bulbTime} ${bulbTime} `)
                .then(()=>{resolve();})
                .catch(reject);
        });
    }
    setDelay(delayArray){
        return new Promise((resolve,reject)=>{
            if(delayArray.length === 6){
                let toSend ='d';
                let ok = true;
                for(let i=0;i<6;i++){
                    if(typeof delayArray[i] ==="number"){
                        toSend+=`${parseInt(delayArray[i]).toString()} `;
                    }else{
                        ok=false;
                    }
                }
                if(ok){
                    this.serial.sendPlaintext(toSend,"setting delay array",isTenFourByteArray,()=>{this.getDelaysState().then(resolve).catch(reject)},7,reject);
                }else{
                    reject("array passed did not contain numbers")
                }
            }else{
                reject("must pass all 6 states - one for each output");
            }
        })
    }
    getOutputModeState(){
        return new Promise((resolve,reject)=>{
            if(this.isMV){
                this.serial.sendPlaintext('/O',"getting outputMode state",(resp)=>{
                    let line = helper.byteArrayToString(resp);
                    return lineIncludes(line,"OutputMode")
                },(response)=>{
                    let line = helper.byteArrayToString(response);
                    let modes = getArrayBetweenBrackets(line);
                    let msg="";
                    let ok = true;
                    if(modes.length === 6){
                        for(let i=0;i<6;i++){
                            let mode = modes[i].toUpperCase();
                            if(mode === "CC"){
                                this.outputMode[i]=0;
                            }else if(mode === "3V3"){
                                this.outputMode[i]=1;
                            }else if(mode === "5V0"){
                                this.outputMode[i]=2;
                            }else{
                                ok=false;
                                msg = `did not recognise setting - ${modes[i]}`;
                            }
                        }
                    }else{
                        msg = "did not get 6 elements back";
                        ok=false;
                    }
                    if(ok){
                        resolve();
                    }else{
                        reject(`could not understand response from triggerbox- ${msg} - ${helper.byteArrayToString(response)}`);
                    }
                },-1,reject)
            }else{
                reject("not an MV triggerbox - cannot set input mode")
            }
        })
    }
    setOutputMode(outputModeArray){
        return new Promise((resolve,reject)=>{
            if(outputModeArray.length === 6){
                let toSend ='/o';
                let ok = true;
                for(let i=0;i<6;i++){
                    switch(parseInt(outputModeArray[i])){
                        case 0:
                            toSend+='0';
                            break;
                        case 1:
                            toSend+='1';
                            break;
                        case 2:
                            toSend+='2';
                            break;
                        default:
                            ok=false;
                            break;
                    }
                }
                if(ok){
                    this.serial.sendPlaintext(toSend,"setting outputMode array",isTenFourByteArray,()=>{this.getOutputModeState().then(resolve).catch(reject)},7,reject);
                }else{
                    reject("array passed did not contain integers corresponding to output modes");
                }
            }else{
                reject("must pass all 6 states - one for each output");
            }
        })
    }
    getInputModeState(){
        return new Promise((resolve,reject)=>{
            if(this.isMV){
                this.serial.sendPlaintext('/I',"getting inputMode state",(resp)=>{
                    let line = helper.byteArrayToString(resp);
                    return lineIncludes(line,"InputMode")
                },(response)=>{
                    let line = helper.byteArrayToString(response);
                    let modes = getArrayBetweenBrackets(line);
                    let ok = true;
                    let msg;
                    if(modes.length === 1){
                        let mode = modes[0].toUpperCase().trim();
                        if(mode === "CC"){
                            this.inputMode=0;
                        }else if(mode === "3V3"){
                            this.inputMode=1;
                        }else if(mode === "5V0"){
                            this.inputMode=2;
                        }else{
                            msg = `did not recognise setting - ${mode}`;
                            ok = false;
                        }
                    }else{
                        msg = "did not get 1 element back";
                        ok=false;
                    }
                    if(ok){resolve();}else{
                        reject(`could not understand response from triggerbox - ${msg} - ${helper.byteArrayToString(response)}`);
                    }
                },-1,reject)
            }else{
                reject("not an MV triggerbox - cannot set input mode")
            }
        })
    }

    setInputMode(inputModeInt){
        return new Promise((resolve,reject)=>{
            let toSend ='/i';
            let ok = true;
            switch(parseInt(inputModeInt)){
                case 0:
                    toSend+='0';
                    break;
                case 1:
                    toSend+='1';
                    break;
                case 2:
                    toSend+='2';
                    break;
                default:
                    ok=false;
                    break;
            }
            if(ok){
                this.serial.sendPlaintext(toSend,"setting input mode",isTenFourByteArray,()=>{this.getInputModeState().then(resolve).catch(reject)},7,reject);
            }else{
                reject("array passed did not contain integers corresponding to output modes");
            }

        })
    }
}

const events = [

    //all triggerboxes
    {
        handle:'save-all-triggerboxes',
        function:(callback)=>{
            console.log("saving all triggerboxes");
            if(TriggerBoxes.length > 0){
                let promiseHolder = [];
                for (let i=0;i<TriggerBoxes.length;i++){
                   promiseHolder.push(TriggerBoxes[i].saveSettings());
                }
               Promise.all(promiseHolder)
                   .then(()=>{
                       callback({status:true});
                   })
                   .catch((e)=>{
                   callback({status:false,msg:e})
               })
            }else{
                callback({status:false,msg:"no TriggerBoxes detected"});
            }
        },
        isPublic: true
    },
    {
        handle:'restart-all-triggerboxes',
        function:(callback)=>{
            console.log("restarting all triggerboxes");
            if(TriggerBoxes.length > 0){
                let promiseHolder = [];
                for (let i=0;i<TriggerBoxes.length;i++){
                   promiseHolder.push(TriggerBoxes[i].restart());
                }
               Promise.all(promiseHolder)
                   .then(()=>{
                       callback({status:true});
                   })
                   .catch((e)=>{
                   callback({status:false,msg:e})
               })
            }else{
                callback({status:false,msg:"no TriggerBoxes detected"});
            }
        },
        isPublic: true
    },
    {
        handle:'factory-reset-all-triggerboxes',
        function:(callback)=>{
            console.log("factory resetting all triggerboxes");
            if(TriggerBoxes.length > 0){
                let promiseHolder = [];
                for (let i=0;i<TriggerBoxes.length;i++){
                   promiseHolder.push(TriggerBoxes[i].factoryReset());
                }
               Promise.all(promiseHolder)
                   .then(()=>{
                       callback({status:true});
                   })
                   .catch((e)=>{
                       callback({status:false,msg:e})
                   })
            }else{
                callback({status:false,msg:"no TriggerBoxes detected"});
            }
        },
        isPublic: true
    },
    {
        handle:'set-shutter-enable-all-triggerboxes',
        function:(shutterArray,callback)=>{
            helper.printInBlue("setting shutter enable array on all triggerboxes");
            if(TriggerBoxes.length > 0){
                let promiseHolder = [];
                for (let i=0;i<TriggerBoxes.length;i++){
                    promiseHolder.push(TriggerBoxes[i].setShutterEnable(shutterArray));
                }
                Promise.all(promiseHolder)
                    .then(()=>{
                        callback({status:true});
                    })
                    .catch((e)=>{
                        callback({status:false,msg:e})
                    })
            }else{
                callback({status:false,msg:"no TriggerBoxes detected"});
            }
        },
        isPublic: true
    },
    {
        handle:'set-focus-enable-all-triggerboxes',
        function:(focusArray,callback)=>{
            helper.printInBlue("setting focus enable array on all triggerboxes");
            if(TriggerBoxes.length > 0){
                let promiseHolder = [];
                for (let i=0;i<TriggerBoxes.length;i++){
                    promiseHolder.push(TriggerBoxes[i].setFocusEnable(focusArray));
                }
                Promise.all(promiseHolder)
                    .then(()=>{
                        callback({status:true});
                    })
                    .catch((e)=>{
                        callback({status:false,msg:e})
                    })
            }else{
                callback({status:false,msg:"no TriggerBoxes detected"});
            }
        },
        isPublic: true
    },
    {
        handle:'set-delay-all-triggerboxes',
        function:(delayArray,callback)=>{
            helper.printInBlue("setting delay on all triggerboxes");
            if(TriggerBoxes.length > 0){
                let promiseHolder = [];
                for (let i=0;i<TriggerBoxes.length;i++){
                    promiseHolder.push(TriggerBoxes[i].setDelay(delayArray));
                }
                Promise.all(promiseHolder)
                    .then(()=>{
                        callback({status:true});
                    })
                    .catch((e)=>{
                        callback({status:false,msg:e})
                    })
            }else{
                callback({status:false,msg:"no TriggerBoxes detected"});
            }
        },
        isPublic: true
    },
    {
        handle:'set-output-mode-all-triggerboxes',
        function:(outputArray,callback)=>{
            helper.printInBlue("setting output mode on all triggerboxes");
            if(TriggerBoxes.length > 0){
                let promiseHolder = [];
                for (let i=0;i<TriggerBoxes.length;i++){
                    promiseHolder.push(TriggerBoxes[i].setOutputMode(outputArray));
                }
                Promise.all(promiseHolder)
                    .then(()=>{
                        callback({status:true});
                    })
                    .catch((e)=>{
                        callback({status:false,msg:e})
                    })
            }else{
                callback({status:false,msg:"no TriggerBoxes detected"});
            }
        },
        isPublic: true
    },
    {
        handle:'set-bulb-time-all-triggerboxes',
        function:(time,callback)=>{
            helper.printInBlue("setting bulb time on all triggerboxes");
            if(TriggerBoxes.length > 0){
                let promiseHolder = [];
                for (let i=0;i<TriggerBoxes.length;i++){
                    promiseHolder.push(TriggerBoxes[i].setBulbTime(time));
                }
                Promise.all(promiseHolder)
                    .then(()=>{
                        callback({status:true});
                    })
                    .catch((e)=>{
                        callback({status:false,msg:e})
                    })
            }else{
                callback({status:false,msg:"no TriggerBoxes detected"});
            }
        },
        isPublic: true
    },
    {
        handle:'set-input-mode-all-triggerboxes',
        function:(inputModeInt,callback)=>{
            helper.printInBlue("setting input mode on all triggerboxes");
            if(TriggerBoxes.length > 0){
                let promiseHolder = [];
                for (let i=0;i<TriggerBoxes.length;i++){
                    promiseHolder.push(TriggerBoxes[i].setInputMode(inputModeInt));
                }
                Promise.all(promiseHolder)
                    .then(()=>{
                        callback({status:true});
                    })
                    .catch((e)=>{
                        callback({status:false,msg:e})
                    })
            }else{
                callback({status:false,msg:"no TriggerBoxes detected"});
            }
        },
        isPublic: true
    },

    //individual
    {
        handle:'save-triggerbox',
        function:(id,callback)=>{
            if(TriggerBoxes.length > 0){
                let found = false;
                for (let i=0;i<TriggerBoxes.length;i++){
                    if(TriggerBoxes[i].id === id){
                        found = true;
                        TriggerBoxes[i].saveSettings().then(()=>{callback({status:true});}).catch((e)=>{
                            callback({status:false,msg:e})
                        });
                    }
                    if(!found){
                        callback({status:false,msg:`could not find triggerbox id ${id}`});
                    }
                }
            }else{
                callback({status:false,msg:"no TriggerBoxes detected"});
            }
        },
        isPublic: false
    },
    {
        handle:'restart-triggerbox',
        function:(id,callback)=>{
            if(TriggerBoxes.length > 0){
                let found = false;
                for (let i=0;i<TriggerBoxes.length;i++){
                    if(TriggerBoxes[i].id === id){
                        found = true;
                        TriggerBoxes[i].restart().then(()=>{callback({status:true});}).catch((e)=>{
                            callback({status:false,msg:e})
                        });
                    }
                    if(!found){
                        callback({status:false,msg:`could not find triggerbox id ${id}`});
                    }
                }
            }else{
                callback({status:false,msg:"no TriggerBoxes detected"});
            }
        },
        isPublic: false
    },
    {
        handle:'factory-reset-triggerbox',
        function:(id,callback)=>{
            if(TriggerBoxes.length > 0){
                let found = false;
                for (let i=0;i<TriggerBoxes.length;i++){
                    if(TriggerBoxes[i].id === id){
                        found = true;
                        TriggerBoxes[i].factoryReset().then(()=>{callback({status:true});}).catch((e)=>{
                            callback({status:false,msg:e})
                        });
                    }
                    if(!found){
                        callback({status:false,msg:`could not find triggerbox id ${id}`});
                    }
                }
            }else{
                callback({status:false,msg:"no TriggerBoxes detected"});
            }
        },
        isPublic: false
    },
    {
        handle:'set-shutter-enable-triggerbox',
        function:(id,payload,callback)=>{
            if(TriggerBoxes.length > 0){
                let found = false;
                for (let i=0;i<TriggerBoxes.length;i++){
                    if(TriggerBoxes[i].id === id){
                        found = true;
                        TriggerBoxes[i].setShutterEnable(payload).then(()=>{callback({status:true});}).catch((e)=>{
                            callback({status:false,msg:e})
                        });
                    }
                    if(!found){
                        callback({status:false,msg:`could not find triggerbox id ${id}`});
                    }
                }
            }else{
                callback({status:false,msg:"no TriggerBoxes detected"});
            }
        },
        isPublic: false
    },
    {
        handle:'set-focus-enable-triggerbox',
        function:(id,payload,callback)=>{
            if(TriggerBoxes.length > 0){
                let found = false;
                for (let i=0;i<TriggerBoxes.length;i++){
                    if(TriggerBoxes[i].id === id){
                        found = true;
                        TriggerBoxes[i].setFocusEnable(payload).then(()=>{callback({status:true});}).catch((e)=>{
                            callback({status:false,msg:e})
                        });
                    }
                    if(!found){
                        callback({status:false,msg:`could not find triggerbox id ${id}`});
                    }
                }
            }else{
                callback({status:false,msg:"no TriggerBoxes detected"});
            }
        },
        isPublic: false
    },
    {
        handle:'set-delay-triggerbox',
        function:(id,payload,callback)=>{
            if(TriggerBoxes.length > 0){
                let found = false;
                for (let i=0;i<TriggerBoxes.length;i++){
                    if(TriggerBoxes[i].id === id){
                        found = true;
                        TriggerBoxes[i].setDelay(payload).then(()=>{callback({status:true});}).catch((e)=>{
                            callback({status:false,msg:e})
                        });
                    }
                    if(!found){
                        callback({status:false,msg:`could not find triggerbox id ${id}`});
                    }
                }
            }else{
                callback({status:false,msg:"no TriggerBoxes detected"});
            }
        },
        isPublic: false
    },
    {
        handle:'set-output-mode-triggerbox',
        function:(id,payload,callback)=>{
            if(TriggerBoxes.length > 0){
                let found = false;
                for (let i=0;i<TriggerBoxes.length;i++){
                    if(TriggerBoxes[i].id === id){
                        found = true;
                        TriggerBoxes[i].setOutputMode(payload).then(()=>{callback({status:true});}).catch((e)=>{
                            callback({status:false,msg:e})
                        });
                    }
                    if(!found){
                        callback({status:false,msg:`could not find triggerbox id ${id}`});
                    }
                }
            }else{
                callback({status:false,msg:"no TriggerBoxes detected"});
            }
        },
        isPublic: false
    },
    {
        handle:'set-input-mode-triggerbox',
        function:(id,inputModeInt,callback)=>{
            if(TriggerBoxes.length > 0){
                let found = false;
                for (let i=0;i<TriggerBoxes.length;i++){
                    if(TriggerBoxes[i].id === id){
                        found = true;
                        TriggerBoxes[i].setInputMode(inputModeInt).then(()=>{callback({status:true});}).catch((e)=>{
                            callback({status:false,msg:e})
                        });
                    }
                    if(!found){
                        callback({status:false,msg:`could not find triggerbox id ${id}`});
                    }
                }
            }else{
                callback({status:false,msg:"no TriggerBoxes detected"});
            }
        },
        isPublic: false
    },


    {
        handle:'get-triggerboxes',
        function:(callback)=>{
            if(TriggerBoxes.length > 0){
                let triggerboxStateData = [];
                for (let i=0;i<TriggerBoxes.length;i++){
                    triggerboxStateData.push(TriggerBoxes[i].reportState())
                }
                callback({status:true,msg:triggerboxStateData});
            }else{
                callback({status:false,msg:"no TriggerBoxes detected"});
            }
        },
        isPublic: false
    },


]




module.exports = {TriggerBox:Triggerbox,events:events,};