module.exports = {
    name:"TriggerBox",
    baudRate:115200,
    idCommandToSend:triggerIDCommand,
    identificationResponses:["TriggerBox:[box_id]"],
    isPlaintext:true,
    parser:parser
}