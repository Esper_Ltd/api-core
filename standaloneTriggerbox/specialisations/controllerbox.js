module.exports = {
    name:"ControllerBox",
    baudRate:115200,
    idCommandToSend:controllerIDCommand,
    identificationResponses:cbRevisions,
    isPlaintext:false,
    parser:parser
}