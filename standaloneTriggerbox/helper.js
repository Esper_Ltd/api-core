let muted = false;

let tickChar = "\u2713";


/** @type {string} */ let logString = "";


// noinspection AssignmentResultUsedJS,JSUnusedGlobalSymbols
module.exports = {
    /**
     * @param {number[]} arrayIn
     * @returns {string}
     */
    byteArrayToString(arrayIn) {
        let stringResp = "";
        for (let byte of arrayIn) {
            stringResp += String.fromCharCode(byte);
        }
        return stringResp;
    },


    /**
     *
     * @param {string} printThis
     * @param {boolean} [forcePrint]
     */
    print(printThis, forcePrint = false) {
        if (typeof backendLogger !== "undefined"){
            if (!muted || forcePrint){
                backendLogger.info(printThis);
            }
            else{
                backendLogger.silly("Muted: " + printThis);
            }
            logString += printThis;
        }
        else{
            console.log(printThis)
            //console.error("ERR: can't print to logger:" + printThis);
        }

    },

    printInRed(printThis) {
        module.exports.print(module.exports.ANSI.RED + printThis + module.exports.ANSI.RESET, true);

        if (typeof printThis === "string") {
            if (printThis.startsWith("ERR:")) {
            }
        }
    },

    printInGreen(printThis) {
        module.exports.print(module.exports.ANSI.GREEN + printThis + module.exports.ANSI.RESET);
        if (printThis.startsWith("SUCC:")){
            console.log(printThis);
        }
        console.log(printThis);
    },

    printInBlue(printThis) {
        if (!muted) {
            module.exports.print(module.exports.ANSI.BLUE + printThis + module.exports.ANSI.RESET);
        }
    },

    /**
     * @function
     * @param {string} printThis
     * @returns void
     */
    printInYellow(printThis) {
        if (!muted) {
            module.exports.print(module.exports.ANSI.YELLOW + printThis + module.exports.ANSI.RESET);
        }
    },

    println(){
        if (!muted){
            module.exports.print("\n");
        }
    },

    uniformSpacing(bytes) {
        let toRet = "";
        for (let b of bytes) {
            if (b >= 100) {
                toRet += b + "|";
            } else if (b >= 10) {
                toRet += " " + b + "|";
            } else {
                toRet += "  " + b + "|";
            }
        }
        toRet = toRet.slice(0, toRet.length - 1);
        return toRet;
    },

    getTickChar(){
        return tickChar;
    },


    numberToPaddedColouredString(perc){
        let roundedPerc = Math.round(perc);
        let str = roundedPerc.toString().padStart(5, " ");
        if (roundedPerc === 0){
            return module.exports.ANSI.BLUE + str + module.exports.ANSI.RESET;
        }
        else if (roundedPerc > 0 && roundedPerc < 32767){
            return module.exports.ANSI.YELLOW + str + module.exports.ANSI.RESET;
        }
        else if (roundedPerc === 32767){
            return module.exports.ANSI.RED + str + module.exports.ANSI.RESET;
        }
        return module.exports.ANSI.RED + "ERROR";
    },



    ANSI: {
        RESET: "\u001B[0m ",
        BLACK: "\u001B[30m ",
        RED: "\033[0;91m ",
        GREEN: "\u001B[32m ",
        YELLOW: "\u001B[33m ",
        BLUE: "\u001B[34m ",
        PURPLE: "\u001B[35m ",
        CYAN: "\u001B[36m ",
        WHITE: "\u001B[37m "
    },


    typist: {
        checkIsByteArray(ba,silent) {
            if (!Array.isArray(ba)) {
                if(!silent){
                    console.trace("typist");
                }
                return false;
            }
            for (let byt of ba) {
                if (typeof byt !== "number") {
                    if(!silent) {
                        console.trace("typist");
                    }
                    return false;
                }
                if (!Number.isInteger(byt)) {
                    if(!silent) {
                        console.trace("typist");
                    }
                    return false;
                }
                if (module.exports.typist.checkNotNaN(byt) === false) {
                    if(!silent) {
                        console.error("TE-byteArray");
                    }
                    return false;
                }
            }
            return true;
        },
        checkNotNaN(wn) {
            if (Number.isNaN(wn)) {
                console.trace("typist");
                return false;
            }
            return true;
        },
        checkIsFloat(isf) {
            if (typeof isf === undefined) {
                console.trace("typist");
                return false;
            }
            if (Number.isNaN(isf)) {
                console.trace("typist");
                return false;
            }
            return true;
        },
        checkIsDescription(idsc) {
            if (idsc) {
                if (typeof idsc !== "string") {
                    if (idsc.length < 2) {
                        console.trace("typist");
                        return false;
                    }
                }
            }
            else {
                console.trace("typist");
                return false;
            }
            return true;
        },
        checkIsFunction(isf) {
            if (isf) {
                if (!(typeof isf === "function")) {
                    console.trace("typist");
                    return false;
                }
            } else {
                console.trace("typist");
                return false;
            }
            return true;
        },
        checkIsInt(isin) {
            if (!(typeof isin === "number")) {
                console.trace("typist");
                return false;
            }
            if (!Number.isInteger(isin)) {
                console.trace("typist");
                return false;
            }
            if (Number.isNaN(isin)) {
                console.trace("typist");
                return false;
            }
            return true;
        },
        checkIsNumericArray(atc) {
            if (atc) {
                if (Array.isArray(atc)) {
                    for (let val of atc) {
                        if (Number.isNaN(val)) {
                            console.trace("typist");
                            return false;
                        }
                        if (typeof val !== "number") {
                            console.trace("typist");
                            return false;
                        }
                    }
                }
                else {
                    console.trace("typist");
                    return false;
                }
            }
            else {
                console.trace("typist");
                return false;
            }
            return true;
        },
        checkIsBoolean(isbol) {
            if (typeof isbol !== "boolean") {
                console.trace("typist");
                return false;
            }
            return true;
        },
        checkIsObject(isObj) {
            if (typeof isObj !== "object") {
                console.trace("typist");
                return false;
            }
            return true;
        },
        checkIsArray(isAry) {
            if (!Array.isArray(isAry)) {
                console.trace("typist");
                return false;
            }
            return true;
        },
    },


};
