const SerialComms = require("./serialComms.js");
const {TriggerBox,events} = require("./triggerbox.js");
global.TriggerBoxes = [];

process.send("starting up");



SerialComms.registerControllerBoxHandler(()=>{
    console.log("registering controllerbox");
});

SerialComms.registerTriggerBoxHandler((tb)=>{
    console.log("found a TB");

    TriggerBoxes.push(new TriggerBox(tb,(TriggerBoxes.length+1),true));
    setTimeout(()=>{process.send("found-triggerbox");},2000);
});
SerialComms.registerProgressFunction(console.log);

SerialComms.start().then(()=>{console.log("serialCommsStartedUp");});


process.on("message",(msg)=> {
    let parsed = JSON.parse(msg);
    console.log(msg)
    let found = false;
    for (let i = 0; i < events.length; i++) {
        let event = events[i];
        if(parsed.handle === event.handle){
            found = true;
            console.log("found handle")

            if(!Object.prototype.hasOwnProperty.call(parsed,"payload")){
                console.log("empty payload");
                event.function(()=>{});
            }else{
                console.log("non - empty payload");
                event.function(parsed.payload,()=>{});
            }
        }
    }
    if(!found){
        console.log(`not found handle ${msg.handle}`);
    }
});