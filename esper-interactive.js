const readline = require('readline');
const { stdin: input, stdout: output } = require('process');
readline.emitKeypressEvents(process.stdin);
const rl = readline.createInterface({ input, output });

rl.on('SIGINT', quit);  // CTRL+C
rl.on('SIGQUIT', quit); // Keyboard quit
rl.on('SIGTERM', quit); // `kill` command

let onShutdown = [];

let registerShutdownCallback = (funct)=>{
    onShutdown.push(funct);
};


function quit(){
    console.log("quitting...");
    for (let i =0;i<onShutdown.length;i++){
        onShutdown[i]();
    }
    process.stdin.setRawMode(false);
    process.exit(0);
}

let entries = [];

/**
 *
 * @param {string} key
 * @param {string} description
 * @param {function} callback
 */
let registerKeypress = (key,description,callback)=>{
    if(key === "q" || key === "Q"){
        console.log("q or Q is already registered for quit, please dont use that key");
        process.exit(1);
    }
    entries.push({key,description,callback});
};


let startInteractive = ()=>{
    entries = [...entries, ...[ {key:"q",description:"quit",callback:quit}, {key:"Q",description:"quit",callback:quit}]];
    for ( let i =0; i < entries.length; i++ ){
        if(entries[i].key === " "){
            console.log(`"spacebar" - ${entries[i].description}`);
        }else if(entries[i].key === "Q"){
            //pass
        }else{
            console.log(`"${entries[i].key}" - ${entries[i].description}`);
        }
    }


    process.stdin.on("keypress", (err, key) => {
        readline.cursorTo(process.stdout, 0);
        let char = key.sequence;
        for ( let i =0; i < entries.length; i++ ){
            if(entries[i].key === char){
                entries[i].callback();
            }
        }
    })
};



module.exports = {register:registerKeypress, start:startInteractive, onShutdown:registerShutdownCallback};